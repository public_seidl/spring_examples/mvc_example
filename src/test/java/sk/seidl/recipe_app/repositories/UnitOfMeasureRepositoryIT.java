package sk.seidl.recipe_app.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import sk.seidl.recipe_app.domains.UnitOfMeasure;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author Matus Seidl (5+3)
 * 2017-12-12
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UnitOfMeasureRepositoryIT {

    @Autowired
    private UnitOfMeasureRepository repository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
   // @DirtiesContext
    public void findByDescription() throws Exception {
        Optional<UnitOfMeasure> optional = repository.findByDescription("Teaspoon");
        assertEquals(true, optional.isPresent());
        assertEquals("Teaspoon",optional.get().getDescription() );
    }
    @Test
    public void findByDescriptionCup() throws Exception {
        Optional<UnitOfMeasure> optional = repository.findByDescription("Cup");
        assertEquals(true, optional.isPresent());
        assertEquals("Cup",optional.get().getDescription() );
    }

}