package sk.seidl.recipe_app.domains;

public enum Difficulty {
    EASY, MODERATE, HARD;
}
