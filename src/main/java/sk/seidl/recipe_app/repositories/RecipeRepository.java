package sk.seidl.recipe_app.repositories;


import org.springframework.data.repository.CrudRepository;
import sk.seidl.recipe_app.domains.Recipe;

/**
 * Created by jt on 6/13/17.
 */
public interface RecipeRepository extends CrudRepository<Recipe, Long> {


}
