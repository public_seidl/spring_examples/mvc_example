package sk.seidl.recipe_app.repositories;


import org.springframework.data.repository.CrudRepository;
import sk.seidl.recipe_app.domains.UnitOfMeasure;

import java.util.Optional;

/**
 * Created by jt on 6/13/17.
 */
public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, Long> {

    Optional<UnitOfMeasure> findByDescription(String description);
}
