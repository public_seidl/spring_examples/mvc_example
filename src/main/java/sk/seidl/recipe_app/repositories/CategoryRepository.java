package sk.seidl.recipe_app.repositories;


import org.springframework.data.repository.CrudRepository;
import sk.seidl.recipe_app.domains.Category;

import java.util.Optional;

/**
 * Created by jt on 6/13/17.
 */
public interface CategoryRepository extends CrudRepository<Category, Long> {

    Optional<Category> findByDescription(String description);
}
